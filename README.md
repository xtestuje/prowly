# A few words about my task: #
1. Tests cases should contain only the functionality of the page and not what the user should do, like click on a particular button.
The reason behind it is that the functionality might stay the same, but the UI might change, example the button can be renamed, removed, etc.
So I tried to stick with writing and testing the page functionality,
2. I've included High priority on all of them because all are important features. More test cases can be made out of the ones provided, like some of the expected results could be on a test case of their own, and of course the priority will be lower then,
3. I've included a type of the tests to point that they are manual. Even though it was obvious that these will be manual tests, I thought it might be good to add them,
4. Requirement field is for requirement traceability purposes and to check if a specific requirement has been traced by a test case. Here we add the A or B based on the test scenarios mentioned,
5. Precondition field was added, just because it is frequently used. Having a certain browser setup and open might count as a precondition.
Also we could've added some other fields like:

    5.1 Environment field where these tests were executed, like type of browser, or if it is production staging etc.

    5.2 Execution time field, where it will tell how long it takes for a test to be executed. It would be useful to track how much time the test run can take
    
    5.3 Status field, where it will tell us the status of the testcase, if it is ready for review, executed, passed, failed, or maybe even if it is archived


## Contact
Iza Michalska - iza.michalska94@gmail.com
